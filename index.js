let users = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista'];
console.log('Original Array:');
console.log(users);

function addNewUser(name) {
  users[users.length] = name;
}
addNewUser('JEPOY DIZON');

console.log(users);

function getUser(index) {
  let itemFound = users[index];
  console.log(itemFound);
}
let itemFound;
itemFound = getUser(1);

function deleteLastUser() {
  let lastItem = users[users.length - 1];
  users.length = users.length - 1;
  return lastItem;
}
let deletedItem = deleteLastUser();
console.log(deletedItem);
console.log(users);

function updateUser(name, index) {
  users[index] = name;
  console.log(users);
}
updateUser('JOPAY', 2);

function deleteAllUsers() {
  users.length = 0;
}
deleteAllUsers();
console.log(users);

function emptyUser() {
  if (users.length > 0) {
    return false;
  } else {
    return true;
  }
}
let isUsersEmpty = emptyUser();
console.log(isUsersEmpty);
